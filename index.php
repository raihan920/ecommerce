<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <title>Nahi's E-Shop</title>
        <link rel="shortcut icon" type="image/x-icon" href="resources/images/default-images/icon.png" />
        <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="resources/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="resources/css/style.css" />
    </head>
    <body >
        <!-- background music -->
        <audio> <!-- autoplay loop -->
         <source src="resources/music/music.mp3" type="audio/mpeg">
         Your browser does not support the audio tag.
        </audio>

        <!-- navigation bar -->
        <nav class="navbar navbar-default navbar-fixed-top" id="menu">
          <div class="container-fluid">
              <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#">Nahi's E-Shop</a>
                </div>

                <div class="collapse navbar-collapse" id="myNavbar">
                  <ul class="nav navbar-nav">
                    <li class="active nav-bar-hover"><a href="#">Home</a></li>
                    <!-- <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">Page 1-1</a></li>
                        <li><a href="#">Page 1-2</a></li>
                        <li><a href="#">Page 1-3</a></li>
                      </ul>
                    </li> -->
                    <li><a class="nav-bar-hover" href="#">All Products</a></li>
                    <li><a class="nav-bar-hover" href="#">My Account</a></li>
                    <li><a class="nav-bar-hover" href="#">Contact Us</a></li>
                    <li><a class="nav-bar-hover" href="#">Shopping Cart <span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                  </ul>

                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="views/customer_panel/register_login.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a id="login_popup" href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                  </ul>

                  <!-- search area -->
                  <form class="search-form navbar-collapse" action="search.php" method="get">
                      <div class="col-md-3">
                        <div class="input-group">
                          <input type="text" id="search-field" class="form-control" name="user_input" placeholder="Search for...">
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="submit" name="search"><span  class="glyphicon glyphicon-search"></span></button>
                          </span>
                        </div><!-- /input-group -->
                      </div><!-- /.col-lg-6 -->
                  </form>
                </div>
            </div>
        </nav>

        <!-- popup login form -->

        <form class="col-md-2 popup_login_form" action="" method="POST">
            <div class="form-group">
                <input type="email" name="email" id="email" class="form-control input-sm" placeholder="Email Address" />
            </div>
            <div class="form-group">
                <input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password" />
            </div>
            <input type="submit" value="Login" class="btn btn-info btn-block" />
        </form>



        <!-- container -->
        <div class="container container-margin-top">
            <!-- header logo-->
            <header class="row">
                <img class="icon" src="resources/images/default-images/icon.png" alt="logo" />
                <img class="logo" src="resources/images/default-images/logo-2.png" alt="logo" />
            </header>

            <div class="row content-area">
                <!-- left content -->
                <div class="col-md-2 zero-padding">
                      <div class="category-brand-label">
                         Categories
                      </div>
                      <ul class="category-brand-ul">
                          <li class=""><a href="#">Laptop</a></li>
                          <li class=""><a href="#">Desktop</a></li>
                          <li class=""><a href="#">Mobile</a></li>
                          <li class=""><a href="#">Tablet</a></li>
                          <li class=""><a href="#">TV</a></li>
                          <li class=""><a href="#">iPhone</a></li>
                          <li class=""><a href="#">Mac</a></li>
                      </ul>
                      <div class="category-brand-label">
                         Brands
                      </div>
                      <ul class="category-brand-ul">
                          <li class=""><a href="#">Toshiba</a></li>
                          <li class=""><a href="#">HP</a></li>
                          <li class=""><a href="#">DELL</a></li>
                          <li class=""><a href="#">Acer</a></li>
                          <li class=""><a href="#">Vio</a></li>
                          <li class=""><a href="#">Lenovo</a></li>
                          <li class=""><a href="#">LG</a></li>
                          <li class=""><a href="#">MSI</a></li>
                          <li class=""><a href="#">Apple</a></li>
                          <li class=""><a href="#">Others...</a></li>
                      </ul>
                </div>
                <!-- right content -->
                <div class="col-md-10">
                  <h3>Column 2</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                  <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                  <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
                </div>
            </div>

            <!-- footer -->
            <footer class="row">
                All right reserved to nahi
            </footer>
          </div>


        <script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
        <script type="text/javascript" src="resources/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="resources/js/script.js"></script>
        <script type="text/javascript">


            $("#login_popup").click(function(){
                // alert("agaergt");
                $(".popup_login_form").slideToggle(400, "swing");
            });
        </script>

    </body>
</html>
