<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Register in Nahi's E-Shop</title>
        <link rel="stylesheet" href="../../resources/css/style.css" />
        <link rel="shortcut icon" type="image/x-icon" href="../../resources/images/default-images/icon.png" />
        <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap-theme.min.css" />
    </head>
    <body>
        <div class="container">
        <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
        	<div class="panel panel-default">
        		<div class="panel-heading">
			    	<h3 class="panel-title">Please Register <small>@ Nahi's E-Shop</small><small class="pull-right">(customer panel)</small></h3>
			 	</div>
	 			<div class="panel-body">
    	    		<form role="form" action="" method="POST">
    	    			<div class="row">
    	    				<div class="col-xs-6 col-sm-6 col-md-6">
    	    					<div class="form-group">
    	                            <input type="text" name="first_name" id="first_name" class="form-control input-sm" placeholder="First Name">
    	    					</div>
    	    				</div>
    	    				<div class="col-xs-6 col-sm-6 col-md-6">
    	    					<div class="form-group">
    	    						<input type="text" name="last_name" id="last_name" class="form-control input-sm" placeholder="Last Name">
    	    					</div>
    	    				</div>
    	    			</div>

    	    			<div class="form-group">
    	    				<input type="email" name="email" id="email" class="form-control input-sm" placeholder="Email Address" />
    	    			</div>

    	    			<div class="row">
    	    				<div class="col-xs-6 col-sm-6 col-md-6">
    	    					<div class="form-group">
    	    						<input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password" onchange="checkPasswordMatch();"/>
    	    					</div>
    	    				</div>
    	    				<div class="col-xs-6 col-sm-6 col-md-6">
    	    					<div class="form-group">
    	    						<input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm Password" onchange="checkPasswordMatch();" />
    	    					</div>
    	    				</div>
    	    			</div>
                        <div class="passwordAlertMessage" id="passwordAlertMessage"> </div>
    	    			<input type="submit" value="Register" class="btn btn-info btn-block" />
    	    		</form>
                    <div class="hr_line_for_or">
                        <span class="or"> or </span>
                    </div>
                    <form class="" action="" method="POST">
                        <div class="form-group">
    	    				<input type="email" name="email" id="email" class="form-control input-sm" placeholder="Email Address" />
    	    			</div>
                        <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password" onchange="checkPasswordMatch();"/>
                        </div>
                        <input type="submit" value="Login" class="btn btn-info btn-block" />
                    </form>
	    	    </div>
	    	</div>
    		</div>
    	</div>
    </div>
    <script type="text/javascript" src="../../resources/js/jquery-3.1.0.min.js"></script>
    <script type="text/javascript" src="../../resources/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../resources/js/script.js"></script>
    <script type="text/javascript">

        $(document).ready(function(){
            $("#passwordAlertMessage").removeClass('alert alert-danger');
            $("#passwordAlertMessage").removeClass('alert alert-success');
            $("#passwordAlertMessage").html("Enter password in both fields");
            $("#passwordAlertMessage").addClass("alert alert-info");
        });

        function checkPasswordMatch(){
            var password = $("#password").val();
            var confirmPassword = $("#password_confirmation").val();

            if (password!="" && confirmPassword!="") {

                if (password != confirmPassword) {
                    $("#passwordAlertMessage").removeClass('alert alert-danger');
                    $("#passwordAlertMessage").removeClass('alert alert-success');
                    $("#passwordAlertMessage").removeClass('alert alert-info');
                    $("#passwordAlertMessage").html("Password did not match");
                    $("#passwordAlertMessage").addClass("alert alert-danger");
                }else {
                    $("#passwordAlertMessage").removeClass('alert alert-danger');
                    $("#passwordAlertMessage").removeClass('alert alert-info');
                    $("#passwordAlertMessage").removeClass('alert alert-success');
                    $("#passwordAlertMessage").html("Password matched");
                    $("#passwordAlertMessage").addClass("alert alert-success");
                }

                $(document).ready(function(){
                    $("#password_confirmation").keyup(checkPasswordMatch);
                });

            }else {
                $("#passwordAlertMessage").removeClass('alert alert-danger');
                $("#passwordAlertMessage").removeClass('alert alert-success');
                $("#passwordAlertMessage").html("Enter password in both fields");
                $("#passwordAlertMessage").addClass("alert alert-info");
            }

        }
    </script>
    </body>
</html>
