<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Admin landing page </title>
        <link rel="stylesheet" href="../../resources/css/style.css" />
        <link rel="shortcut icon" type="image/x-icon" href="../../resources/images/default-images/icon.png" />
        <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap-theme.min.css" />
    </head>
    <body>

        <!-- navigation bar -->
        <nav class="navbar navbar-default navbar-fixed-top" id="menu">
          <div class="container-fluid">
              <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#">Nahi's E-Shop</a>
                </div>

                <div class="collapse navbar-collapse" id="myNavbar">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a id="login_popup" href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
                  </ul>
                </div>
            </div>
        </nav>

        <div class="container container-top-margin">
            <div class="row text-center">
                <h2 class="admin-panel-div">Admin Panel</h2>
            </div>
            <div class="row container-top-margin">
                <p>
                    <a href="new_category.php" class="btn btn-default btn-lg">Add New Category  <span class="glyphicon glyphicon-menu-hamburger"></span></a>
                </p>
                <p>
                    <a href="new_brand.php" class="btn btn-default btn-lg">Add New Brand  <span class="glyphicon glyphicon-bitcoin"></span></a>
                </p>
            </div>
        </div>




        <script type="text/javascript" src="../../resources/js/jquery-3.1.0.min.js"></script>
        <script type="text/javascript" src="../../resources/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../resources/js/script.js"></script>
        <script type="text/javascript">

        </script>
    </body>
</html>
